package ru.stuvanya.lwe;

import java.util.HashMap;
import java.util.HashSet;

import org.bukkit.entity.Player;

import com.sk89q.worldedit.WorldEditException;
import com.sk89q.worldedit.extent.AbstractDelegateExtent;
import com.sk89q.worldedit.extent.Extent;
import com.sk89q.worldedit.math.BlockVector2;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.world.biome.BiomeType;
import com.sk89q.worldedit.world.block.BlockStateHolder;

public class WEExtent extends AbstractDelegateExtent
{
	private HashSet<RegionWrapper> mask;
	private Player player;
	static HashMap<Player, Long> timeouts = new HashMap<Player, Long>();
	static String denyMessage = LimitedWorldEdit.plugin.getConfig().getString("denyMessage").replace("&", "§");

	public WEExtent(HashSet<RegionWrapper> mask, Extent extent, Player player) {
		super(extent);
		this.mask = mask;
		this.player = player;
	}

    @Override
    public <T extends BlockStateHolder<T>> boolean setBlock(BlockVector3 location, T block) throws WorldEditException {
    	if (WEManager.maskContains(this.mask, location.getX(), location.getY(), location.getZ())) {
    		return super.setBlock(location, block);
    	} else {
    		if (!timeouts.containsKey(player) || ((System.currentTimeMillis() - timeouts.get(player)) > 1000)) {
    			player.sendMessage(denyMessage);
    		}
    			
    		timeouts.put(player, System.currentTimeMillis());
    		return false;
    	}
	}
    
    @Override
    public boolean setBiome(BlockVector2 position, BiomeType biome) {
    	if (WEManager.maskContains(this.mask, position.getBlockX(), position.getBlockZ())) {
    		return super.setBiome(position, biome);
    	} else {
    		if (!timeouts.containsKey(player) || ((System.currentTimeMillis() - timeouts.get(player)) > 1000)) {
    			player.sendMessage(denyMessage);
    		}
    			
    		timeouts.put(player, System.currentTimeMillis());
    		return false;
    	}
    }
}
