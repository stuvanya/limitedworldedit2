package ru.stuvanya.lwe;

import java.util.HashMap;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

public class PermissionManager {
	
	private static HashMap<String, Integer> limits = new HashMap<String, Integer>();
	private static int defaultLimit;
	
	public static int getLimit(Player p) {
		int limit = defaultLimit;
		for (String perm : limits.keySet()) {
			if (p.hasPermission(perm) && (limits.get(perm) > limit)) {
				limit = limits.get(perm);
			}
		}
		return limit;
	}

	public static void init() {
		ConfigurationSection limit = LimitedWorldEdit.plugin.getConfig().getConfigurationSection("limits");
		defaultLimit = LimitedWorldEdit.plugin.getConfig().getInt("defaultLimit", 100);
		
		for (String s : limit.getKeys(false)) {
			limits.put(s, limit.getInt("s"));
		}
		
	}
}
