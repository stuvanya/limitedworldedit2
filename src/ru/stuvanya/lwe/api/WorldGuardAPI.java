package ru.stuvanya.lwe.api;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.entity.Player;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import ru.stuvanya.lwe.RegionWrapper;

public class WorldGuardAPI
{
	public static List<RegionWrapper> getRegions(Player player) {
		List<RegionWrapper> output = new ArrayList<RegionWrapper>();
		RegionManager mgr = WorldGuard.getInstance().getPlatform().getRegionContainer().get(BukkitAdapter.adapt(player.getWorld()));
		Collection<ProtectedRegion> values = mgr.getRegions().values();
		for (ProtectedRegion value : values) {
			if (value.getOwners().contains(player.getUniqueId())) {
				BlockVector3 minimumPoint = value.getMinimumPoint();
				BlockVector3 maximumPoint = value.getMaximumPoint();
				RegionWrapper regionWrapper = new RegionWrapper(
						minimumPoint.getBlockX(), maximumPoint.getBlockX(), 
						minimumPoint.getBlockY(), maximumPoint.getBlockY(), 
						minimumPoint.getBlockZ(), maximumPoint.getBlockZ()
						);
				output.add(regionWrapper);
			}
		}
		return output;
	}
}
