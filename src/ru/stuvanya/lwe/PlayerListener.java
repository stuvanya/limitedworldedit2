package ru.stuvanya.lwe;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerListener implements Listener {

	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		if (WEExtent.timeouts.containsKey(e.getPlayer()))
			WEExtent.timeouts.remove(e.getPlayer());
	}
}
