package ru.stuvanya.lwe;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;

public class LimitedWorldEdit extends JavaPlugin
{
	public static WorldEditPlugin worldEdit;
	public static LimitedWorldEdit plugin;

	public void onEnable() {
		WorldEdit.getInstance().getEventBus().register(new LimitedWorldEditListener());
		Bukkit.getServer().getPluginManager().registerEvents(new PlayerListener(), this);
		plugin = this;
		saveDefaultConfig();
		PermissionManager.init();
	}
}
